import requests, http

class Note:

    @staticmethod
    def fetchNotes(protocol:str, provider:str, author:str, project:str, iid:int, key:str, arguments:list = []):
        target = "{0}%2F{1}".format(author, project)
        url = "%s://%s/api/v4/projects/%s/issues/%i/notes" % (protocol, provider, target, iid)
        if len(arguments) > 0:
            url = "%s?" % url
            for index in range(len(arguments)):
                if index > 0:
                    url = "%s&" % url
                url = "%s%s=%s" % (url, arguments[index]["key"], arguments[index]["value"])
        try:
            request = requests.get(url, headers={"PRIVATE-TOKEN":key})
            if request.status_code == http.HTTPStatus.OK:
                return request.json()
        except:
            pass
        return None

    @staticmethod
    def submitNote(protocol:str, provider:str, author:str, project:str, iid:int, key:str, body:str):
        data = {
            "body":body
        }
        target = "{0}%2F{1}".format(author, project)
        url = "%s://%s/api/v4/projects/%s/issues/%i/notes" % (protocol, provider, target, iid)
        try:
            request = requests.post(url, json=data, headers={"PRIVATE-TOKEN":key})
            if request.status_code == http.HTTPStatus.CREATED:
                return request.json()
        except:
            pass
        return False

    @staticmethod
    def editNote(protocol:str, provider:str, author:str, project:str, iid:str, key:str, id:int, body:str=None):
        target = "{0}%2F{1}".format(author, project)
        url = "%s://%s/api/v4/projects/%s/issues/%i/notes/%i" % (protocol, provider, target, iid, id)
        data = {}
        if body:
            data["body"] = body
        try:
            request = requests.put(url, json=data, headers={"PRIVATE-TOKEN":key})
            if request.status_code == http.HTTPStatus.OK:
                return request.json()
        except:
            pass
        return False

    @staticmethod
    def removeNote(protocol:str, provider:str, author:str, project:str, iid:str, key:str, id:int):
        target = "{0}%2F{1}".format(author, project)
        url = "%s://%s/api/v4/projects/%s/issues/%i/notes/%i" % (protocol, provider, target, iid, id)
        try:
            request = requests.delete(url, headers={"PRIVATE-TOKEN":key})
            if request.status_code == http.HTTPStatus.NO_CONTENT:
                return request.json()
        except:
            pass
        return False