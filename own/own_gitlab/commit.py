import requests, http

class Commit:

    @staticmethod
    def fetchCommit(protocol:str, provider:str, author:str, project:str, key:str, arguments:list = []):
        target = "{0}%2F{1}".format(author, project)
        url = "%s://%s/api/v4/projects/%s/repository/commits" % (protocol, provider, target)
        if len(arguments) > 0:
            url = "%s?" % url
            for index in range(len(arguments)):
                if index > 0:
                    url = "%s&" % url
                url = "%s%s=%s" % (url, arguments[index]["key"], arguments[index]["value"])
        try:
            request = requests.get(url, headers={"PRIVATE-TOKEN":key})
            if request.status_code == http.HTTPStatus.OK:
                return request.json()
        except:
            pass
        return None