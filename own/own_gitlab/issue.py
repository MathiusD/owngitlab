import requests, http
from .notes import Note

class Issue:

    @staticmethod
    def submitIssue(protocol:str, provider:str, author:str, project:str, key:str, title:str, description:str, labels:list = []):
        data = {
            "title":title,
            "description":description,
            "labels":""
        }
        for label in labels:
            data["labels"] = "%s%s," % (data["labels"], label)
        target = "{0}%2F{1}".format(author, project)
        url = "%s://%s/api/v4/projects/%s/issues" % (protocol, provider, target)
        try:
            request = requests.post(url, json=data, headers={"PRIVATE-TOKEN":key})
            if request.status_code == http.HTTPStatus.CREATED:
                return request.json()
        except:
            pass
        return False

    @staticmethod
    def editIssue(protocol:str, provider:str, author:str, project:str, key:str, iid:str, title:str=None, description:str=None):
        target = "{0}%2F{1}".format(author, project)
        url = "%s://%s/api/v4/projects/%s/issues/%i" % (protocol, provider, target, iid)
        data = {}
        if title:
            data["title"] = title
        if description:
            data["description"] = description
        try:
            request = requests.put(url, json=data, headers={"PRIVATE-TOKEN":key})
            if request.status_code == http.HTTPStatus.OK:
                return request.json()
        except:
            pass
        return False

    @staticmethod
    def removeIssue(protocol:str, provider:str, author:str, project:str, key:str, iid:str):
        target = "{0}%2F{1}".format(author, project)
        url = "%s://%s/api/v4/projects/%s/issues/%i" % (protocol, provider, target, iid)
        try:
            request = requests.delete(url, headers={"PRIVATE-TOKEN":key})
            if request.status_code == http.HTTPStatus.NO_CONTENT:
                return request.json()
        except:
            pass
        return False

    @staticmethod
    def fetchIssues(protocol:str, provider:str, author:str, project:str, key:str, arguments:list = []):
        target = "{0}%2F{1}".format(author, project)
        url = "%s://%s/api/v4/projects/%s/issues" % (protocol, provider, target)
        if len(arguments) > 0:
            url = "%s?" % url
            for index in range(len(arguments)):
                if index > 0:
                    url = "%s&" % url
                url = "%s%s=%s" % (url, arguments[index]["key"], arguments[index]["value"])
        try:
            request = requests.get(url, headers={"PRIVATE-TOKEN":key})
            if request.status_code == http.HTTPStatus.OK:
                data = request.json()
                for dat in data:
                    if "iid" in dat.keys():
                        notes = Note.fetchNotes(protocol, provider, author, project, dat["iid"], key)
                        dat["notes"] = notes
                return data
        except:
            pass
        return None
